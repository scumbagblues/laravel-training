<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OficinaServicio extends Model
{
    protected $table = 'oficinas_servicios';

    protected $fillable = [
       'oficinas_id',
       'servicios_id'
    ];
}
