@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="alert-danger">
            @foreach($errors->all() as $error)
                <p>{{ $error }}</p>
            @endforeach
        </div>
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">Registrar Cliente</div>

                    <div class="card-body">
                        <form action="{{ route('clientes.update', $registro->id)}}" method="post">
                            @csrf
                            {{ method_field('PUT') }}
                            <div class="form-group">
                                <label for="name">Nombre</label>
                                <input type="text" name="nombre" class="form-control" value="{{ $registro->nombre }}"><br>

                                <label for="name">Nombre</label>
                                <input type="text" name="rfc" class="form-control" value="{{ $registro->rfc }}"><br>

                                <label for="name">Nombre</label>
                                <input type="text" name="email" class="form-control" value="{{ $registro->email }}"><br>

                                <label for="description">Descripcion</label>
                                <input type="text" name="telefono" class="form-control" value="{{ $registro->telefono }}">
                            </div>
                            <button type="submit" class="btn btn-primary">Submit</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection