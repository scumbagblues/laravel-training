<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Planes extends Model
{
    //
        protected $fillable = [
            'nombre',
            'descripcion',
            'precio',
            'cantidad',
            'tipo_tiempo'
        ];
}
