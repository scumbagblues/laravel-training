<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOficinasServiciosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('oficinas_servicios', function (Blueprint $table) {

            $table->integer('oficinas_id')->unsigned();
            $table->integer('servicios_id')->unsigned();

            $table->foreign('oficinas_id')->references('id')->on('oficinas');
            $table->foreign('servicios_id')->references('id')->on('servicios');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('oficinas_servicios');
    }
}
