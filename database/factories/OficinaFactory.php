<?php

use Faker\Generator as Faker;
use App\Oficina;
$factory->define(Oficina::class, function (Faker $faker) {
    return [
        'nombre' => $faker->name,
        'capacidad' => rand(1,10),
        'status' => $faker->randomElement(['Libre', 'Ocupado'])
    ];
});
