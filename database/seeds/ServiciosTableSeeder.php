<?php

use Illuminate\Database\Seeder;

class ServiciosTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                'nombre' => 'baño',
                'descripcion' => 'Baño mixto solo mingitorio'
            ],
            [
                'nombre' => 'Wifi',
                'descripcion' => 'Wifi a no mas de 1 metro de distancia'
            ],
            [
                'nombre' => 'Sala de juntas',
                'descripcion' => 'Sala de juntas para 2 personas'
            ]
        ];

        foreach($data as $item){
            factory(\App\Servicios::class, 1)->create($item);
        }
    }
}
