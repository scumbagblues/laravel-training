<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePagosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pagos', function (Blueprint $table) {
            $table->increments('id');
            $table->double('cantidad');
            $table->integer('agenda_id')->unsigned();
            $table->integer('agenda_clientes_id')->unsigned();
            $table->integer('agenda_planes_id')->unsigned();
            $table->integer('agenda_oficinas_id')->unsigned();
            $table->timestamp('fecha_pago')->nullable();


            $table->foreign('agenda_id')->references('id')->on('agenda');
            $table->foreign('agenda_clientes_id')->references('clientes_id')->on('agenda');
            $table->foreign('agenda_planes_id')->references('planes_id')->on('agenda');
            $table->foreign('agenda_oficinas_id')->references('oficinas_id')->on('agenda');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pagos');
    }
}
