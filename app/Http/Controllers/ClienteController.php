<?php

namespace App\Http\Controllers;

use App\Http\Requests\UpdateClientesRequest;
use Illuminate\Http\Request;
use App\Cliente;

class ClienteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $clientes = Cliente::all();
        return view('clientes/index',compact('clientes'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $registro = Cliente::find($id);
        return view('clientes/edit',compact('registro'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateClientesRequest $request, $id)
    {
        $data = $request->all();
        $rowUpdated = Cliente::find($id)->update($data);

        if($rowUpdated){
            return redirect()->route('clientes')
                ->with('success', "Cliente {$id} editado exitosamente");
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $registro = Cliente::find($id);
        return view('clientes/destroy',compact('registro'));
    }

    public function delete($id){
        if($rowDeleted = Cliente::destroy($id)){
            return redirect()->route('clientes')
                ->with('success', "Cliente {$id} Eliminado exitosamente");
        }
    }
}
