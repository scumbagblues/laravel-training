<?php

use Illuminate\Database\Seeder;

class PlanesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
        [
            'nombre' => 'Plan chido',
            'descripcion' => 'Un plan completo',
            'precio' => 3500.00,
            'cantidad' => 3,
            'tipo_tiempo' => 'semana'
         ],
         [
             'nombre' => 'Plan dental',
             'descripcion' => 'Lisa necesita frenos',
             'precio' => 55000.00,
             'cantidad' => 2,
             'tipo_tiempo' => 'año'
         ],
         [
            'nombre' => 'Plan Premium',
            'descripcion' => 'Incluye servicio de catering',
            'precio' => 75000.00,
            'cantidad' => 1,
            'tipo_tiempo' => 'mes'
         ]
         ];

        foreach($data as $item){
            factory(\App\Planes::class,1)->create($item);
        }
    }
}
