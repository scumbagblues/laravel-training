@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="alert-danger">
            @foreach($errors->all() as $error)
                <p>{{ $error }}</p>
            @endforeach
        </div>
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">Registrar servicio</div>

                    <div class="card-body">
                        <form action="{{ route('servicios.delete', $registro->id)}}" method="post">
                            @csrf
                            {{ method_field('DELETE') }}
                            <div class="form-group">
                                <label for="name">Nombre</label>
                                <input type="text" name="nombre" class="form-control" value="{{ $registro->nombre }}"><br>

                                <label for="description">Descripcion</label>
                                <input type="text" name="descripcion" class="form-control" value="{{ $registro->descripcion }}">
                            </div>
                            <button type="submit" class="btn btn-primary">Submit</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection