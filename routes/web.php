<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

//Route::get('/users', 'UserController@index')->middleware('auth.basic')->name('users');
//Route::get('/users', 'UserController@index')->middleware('auth')->name('users');
Route::group(['middleware' => ['auth']], function () {
    Route::get('/users', 'UserController@index')->name('users');
    Route::get('/clientes', 'ClienteController@index')->name('clientes');
    Route::get('/oficinas', 'OficinaController@index')->name('oficinas');
    Route::get('/planes', 'PlanController@index')->name('planes');

    Route::get('/servicios', 'ServicioController@index')->name('servicios');
    Route::get('/servicios/edit/{id}','ServicioController@edit')->name('servicios.edit');
    Route::get('/servicios/create', 'ServicioController@create')->name('servicios.create');
    Route::post('/servicios/store', 'ServicioController@store')->name('servicios.store');
    Route::put('/servicios/update/{id}', 'ServicioController@update')->name('servicios.update');
    Route::get('/servicios/destroy/{id}', 'ServicioController@destroy')->name('servicios.destroy');
    Route::delete('/servicios/delete/{id}', 'ServicioController@delete')->name('servicios.delete');

    Route::get('/clientes/edit/{id}','ClienteController@edit')->name('clientes.edit');
    Route::put('/clientes/update/{id}', 'ClienteController@update')->name('clientes.update');
    Route::get('/clientes/destroy/{id}', 'ClienteController@destroy')->name('clientes.destroy');
    Route::delete('/clientes/delete/{id}', 'ClienteController@delete')->name('clientes.delete');

});
