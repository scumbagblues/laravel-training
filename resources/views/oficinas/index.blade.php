@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">Clientes</div>

                    <div class="card-body">
                        <table class="table table-bordered table-striped">
                            <thead>
                            <th>Name</th>
                            <th>Capacity</th>
                            <th>Status</th>

                            </thead>
                            <tbody>
                            @foreach($oficinas as $oficina)
                                <tr>
                                    <td>{{ $oficina->nombre }}</td>
                                    <td>{{ $oficina->capacidad }}</td>
                                    <td>{{ $oficina->status }}</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection