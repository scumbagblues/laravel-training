<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Agenda extends Model
{
    use SoftDeletes;

    protected $table = 'agenda';

    protected $fillable = [
      'clientes_id',
      'planes_id',
      'oficinas_id',
      'fecha_inicio',
      'fecha_final'
    ];
}
