@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="alert-danger">
            @foreach($errors->all() as $error)
                <p>{{ $error }}</p>
            @endforeach
        </div>
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">Registrar servicio</div>

                    <div class="card-body">
                        <form action="{{ route('servicios.store')}}" method="post">
                            @csrf

                            <div class="form-group">
                                <label for="name">Nombre</label>
                                <input type="text" name="nombre" class="form-control"><br>

                                <label for="description">Descripcion</label>
                                <input type="text" name="descripcion" class="form-control">
                            </div>
                            <button type="submit" class="btn btn-primary">Submit</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection