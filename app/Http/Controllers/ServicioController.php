<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateServiciosRequest;
use App\Http\Requests\UpdateServiciosRequest;
use App\Servicios;

class ServicioController extends Controller
{
    public function index()
    {
        $servicios = Servicios::all();

        return view('servicios/index', compact('servicios'));
    }

    public function create()
    {
        return view('servicios/create');
    }

    public function store(CreateServiciosRequest $request)
    {
        //dd($request->nombre);
        //$datos = ['nombre' => $request->nombre, 'descripcion' => $request->descripcion];
        //$servicios = new Servicios;
        //$servicios->nombre = $request->nombre;
        //$servicios->descripcion = $request->descripcion;
        //$servicios->save();


        Servicios::create($request->all());

        return redirect()->route('servicios')
            ->with('success', "Servicio creado exitosamente");

    }

    public function edit($id)
    {
        $registro = Servicios::find($id);
        return view('servicios/edit',compact('registro'));
    }

    public function update(UpdateServiciosRequest $request, $id)
    {
        $data = $request->all();
        $rowUpdated = Servicios::find($id)->update($data);
        //dd($rowUpdated);

        if ($rowUpdated){
            return redirect()->route('servicios')
                ->with('success', "Servicio {$id} editado exitosamente");
        }

    }

    public function destroy($id){
        $registro = Servicios::find($id);
        return view('servicios/destroy',compact('registro'));
    }

    public function delete($id){

        if($rowDeleted = Servicios::destroy($id)){
            return redirect()->route('servicios')
                ->with('success', "Servicio {$id} borrado exitosamente");
        }
    }
}