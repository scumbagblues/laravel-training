<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAgendaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('agenda', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('clientes_id')->unsigned();
            $table->integer('planes_id')->unsigned();
            $table->integer('oficinas_id')->unsigned();
            $table->timestamp('fecha_inicio')->nullable();
            $table->timestamp('fecha_final')->nullable();


            $table->foreign('clientes_id')->references('id')->on('clientes');
            $table->foreign('planes_id')->references('id')->on('planes');
            $table->foreign('oficinas_id')->references('id')->on('oficinas');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('agenda');
    }
}
