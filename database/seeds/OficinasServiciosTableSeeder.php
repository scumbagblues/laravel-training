<?php

use Illuminate\Database\Seeder;

class OficinasServiciosTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                'oficinas_id' => 1,
                'servicios_id' => 1
            ]
        ];

        foreach ($data as $item) {
            factory(\App\OficinaServicio::class, 1)->create($item);
        }
    }
}
