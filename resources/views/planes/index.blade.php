@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">Planes</div>

                    <div class="card-body">
                        <table class="table table-bordered table-striped">
                            <thead>
                            <th>Name</th>
                            <th>Description</th>
                            <th>Price</th>
                            <th>Quantity</th>
                            <th>Time </th>

                            </thead>
                            <tbody>
                            @foreach($planes as $plan)
                                <tr>
                                    <td>{{ $plan->nombre }}</td>
                                    <td>{{ $plan->descripcion }}</td>
                                    <td>{{ $plan->precio }}</td>
                                    <td>{{ $plan->cantidad }}</td>
                                    <td>{{ $plan->tipo_tiempo }}</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection