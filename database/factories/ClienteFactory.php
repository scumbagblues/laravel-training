<?php

use Faker\Generator as Faker;
use App\Cliente;

$factory->define(Cliente::class, function (Faker $faker) {
    return [
        'nombre'=> $faker->name,
        'rfc' => str_random(12),
        'email' => $faker->unique()->safeEmail,
        'telefono' => $faker->phoneNumber
    ];
});
