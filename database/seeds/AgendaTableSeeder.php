<?php

use Illuminate\Database\Seeder;

class AgendaTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                'clientes_id' => 1,
                'planes_id' => 1,
                'oficinas_id' => 1,
                'fecha_inicio' => '2018-10-31 16:53:51',
                'fecha_final' => '2018-11-30 16:53:51'
            ]
        ];

        foreach ($data as $item) {
            factory(\App\Agenda::class, 1)->create($item);
        }
    }
}
